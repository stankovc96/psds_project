#process for getting script file directory
variable dispScriptFile [file normalize [info script]]
proc getScriptDirectory {} {
    variable dispScriptFile
    set scriptFolder [file dirname $dispScriptFile]
    return $scriptFolder
}

#change working directory to script file directory
cd [getScriptDirectory]
#set result directory
set resultDir .\/result
#set ip_repo_path to script dir
set ip_repo_path [getScriptDirectory]\/..\/
#redifine resultDir HERE if needed
#set resutDir C:\/User\/result

file mkdir $resultDir

# CONNECT SYSTEM
create_project arps $resultDir  -part xc7z010clg400-1 -force
set_property board_part digilentinc.com:zybo-z7-10:part0:1.0 [current_project]
set_property target_language VHDL [current_project]
create_bd_design "arps_bl_dsg"
update_compile_order -fileset sources_1

#add ip-s to main repo
set_property  ip_repo_paths  $ip_repo_path [current_project]
update_ip_catalog

add_files -fileset constrs_1 -norecurse Top.xdc


#add and configure zynq
startgroup
create_bd_cell -type ip -vlnv xilinx.com:ip:processing_system7:5.5 processing_system7_0
endgroup

set_property -dict [list CONFIG.PCW_FPGA0_PERIPHERAL_FREQMHZ {100} CONFIG.PCW_USE_FABRIC_INTERRUPT {1} CONFIG.PCW_IRQ_F2P_INTR {1}] [get_bd_cells processing_system7_0]
startgroup

apply_bd_automation -rule xilinx.com:bd_rule:processing_system7 -config {make_external "FIXED_IO, DDR" apply_board_preset "1" Master "Disable" Slave "Disable" }  [get_bd_cells processing_system7_0]
endgroup

#ADD HDMI
startgroup

# Set Zynq PL clock to 100MHz and add HP1 slave port
set_property -dict [list CONFIG.PCW_FPGA0_PERIPHERAL_FREQMHZ {100}] [get_bd_cells processing_system7_0]
set_property -dict [list CONFIG.PCW_USE_S_AXI_HP1 {1}] [get_bd_cells processing_system7_0]

# Create hierarchy for HDMI
create_bd_cell -type hier hdmi

# Create IP cores
create_bd_cell -type ip -vlnv digilentinc.com:ip:axi_dynclk:1.0 hdmi/axi_dynclk_0
create_bd_cell -type ip -vlnv xilinx.com:ip:axi_vdma:6.3 hdmi/axi_vdma_0
create_bd_cell -type ip -vlnv xilinx.com:ip:v_tc:6.1 hdmi/v_tc_0
create_bd_cell -type ip -vlnv xilinx.com:ip:v_axi4s_vid_out:4.0 hdmi/v_axi4s_vid_out_0
create_bd_cell -type ip -vlnv digilentinc.com:ip:rgb2dvi:1.4 hdmi/rgb2dvi_0
create_bd_cell -type ip -vlnv xilinx.com:ip:axis_subset_converter:1.1 hdmi/axis_subset_converter_0
create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 hdmi/xlconstant_0

# Set IP core options
set_property -dict [list CONFIG.c_num_fstores {2} CONFIG.c_mm2s_genlock_mode {0} CONFIG.c_s2mm_genlock_mode {0} CONFIG.c_mm2s_linebuffer_depth {2048} CONFIG.c_mm2s_max_burst_length {32} CONFIG.c_include_s2mm {0}] [get_bd_cells hdmi/axi_vdma_0]
set_property -dict [list CONFIG.enable_detection {false}] [get_bd_cells hdmi/v_tc_0]
set_property -dict [list CONFIG.C_HAS_ASYNC_CLK {1} CONFIG.C_ADDR_WIDTH {12} CONFIG.C_VTG_MASTER_SLAVE {1}] [get_bd_cells hdmi/v_axi4s_vid_out_0]
set_property -dict [list CONFIG.kRstActiveHigh {false} CONFIG.kGenerateSerialClk {false}] [get_bd_cells hdmi/rgb2dvi_0]
set_property -dict [list CONFIG.S_TDATA_NUM_BYTES.VALUE_SRC USER CONFIG.M_TDATA_NUM_BYTES.VALUE_SRC USER] [get_bd_cells hdmi/axis_subset_converter_0]
set_property -dict [list CONFIG.S_TDATA_NUM_BYTES {4} CONFIG.M_TDATA_NUM_BYTES {3} CONFIG.TDATA_REMAP {tdata[23:16],tdata[7:0],tdata[15:8]}] [get_bd_cells hdmi/axis_subset_converter_0]
set_property -dict [list CONFIG.CONST_VAL {0}] [get_bd_cells hdmi/xlconstant_0]

# Create HDMI output interface
create_bd_intf_pin -mode Master -vlnv digilentinc.com:interface:tmds_rtl:1.0 hdmi/hdmi_out

# Connect up internal nets
connect_bd_net [get_bd_pins hdmi/axi_dynclk_0/REF_CLK_I] [get_bd_pins hdmi/axi_dynclk_0/s00_axi_aclk]
connect_bd_net [get_bd_pins hdmi/axi_dynclk_0/PXL_CLK_O] [get_bd_pins hdmi/v_tc_0/clk] [get_bd_pins hdmi/v_axi4s_vid_out_0/vid_io_out_clk] [get_bd_pins hdmi/rgb2dvi_0/PixelClk]
connect_bd_net [get_bd_pins hdmi/axi_dynclk_0/PXL_CLK_5X_O] [get_bd_pins hdmi/rgb2dvi_0/SerialClk]
connect_bd_net [get_bd_pins hdmi/axi_dynclk_0/LOCKED_O] [get_bd_pins hdmi/rgb2dvi_0/aRst_n]
connect_bd_net [get_bd_pins hdmi/axis_subset_converter_0/aclk] [get_bd_pins hdmi/axi_vdma_0/m_axis_mm2s_aclk]
connect_bd_net [get_bd_pins hdmi/xlconstant_0/dout] [get_bd_pins hdmi/axis_subset_converter_0/aresetn]
connect_bd_intf_net [get_bd_intf_pins hdmi/v_tc_0/vtiming_out] [get_bd_intf_pins hdmi/v_axi4s_vid_out_0/vtiming_in]
connect_bd_intf_net [get_bd_intf_pins hdmi/axi_vdma_0/M_AXIS_MM2S] [get_bd_intf_pins hdmi/axis_subset_converter_0/S_AXIS]
connect_bd_intf_net [get_bd_intf_pins hdmi/axis_subset_converter_0/M_AXIS] [get_bd_intf_pins hdmi/v_axi4s_vid_out_0/video_in]
connect_bd_intf_net [get_bd_intf_pins hdmi/v_axi4s_vid_out_0/vid_io_out] [get_bd_intf_pins hdmi/rgb2dvi_0/RGB]
connect_bd_intf_net [get_bd_intf_pins hdmi/hdmi_out] [get_bd_intf_pins hdmi/rgb2dvi_0/TMDS]

# Create and connect external HDMI interface
create_bd_intf_port -mode Master -vlnv digilentinc.com:interface:tmds_rtl:1.0 hdmi_out
connect_bd_intf_net [get_bd_intf_ports hdmi_out] -boundary_type upper [get_bd_intf_pins hdmi/hdmi_out]

# Tidy up routing
regenerate_bd_layout -routing

endgroup
#-------------------------------------------------
startgroup
apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config { Clk_master {Auto} Clk_slave {Auto} Clk_xbar {Auto} Master {/hdmi/axi_vdma_0/M_AXI_MM2S} Slave {/processing_system7_0/S_AXI_HP1} intc_ip {Auto} master_apm {0}}  [get_bd_intf_pins processing_system7_0/S_AXI_HP1]
#INFO: [Ipptcl 7-1463] No Compatible Board Interface found. Board Tab not created in customize GUI
#</processing_system7_0/S_AXI_HP1/HP1_DDR_LOWOCM> is being mapped into </hdmi/axi_vdma_0/Data_MM2S> at <0x00000000 [ 1G ]>
apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config { Clk_master {Auto} Clk_slave {Auto} Clk_xbar {Auto} Master {/processing_system7_0/M_AXI_GP0} Slave {/hdmi/axi_dynclk_0/s00_axi} intc_ip {New AXI Interconnect} master_apm {0}}  [get_bd_intf_pins hdmi/axi_dynclk_0/s00_axi]
#</hdmi/axi_dynclk_0/s00_axi/reg0> is being mapped into </processing_system7_0/Data> at <0x43C00000 [ 64K ]>
#INFO: [BD 5-455] Automation on '/hdmi/axi_dynclk_0/REF_CLK_I' will not be run, since it is obsolete due to previously run automations
apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config { Clk_master {Auto} Clk_slave {Auto} Clk_xbar {Auto} Master {/processing_system7_0/M_AXI_GP0} Slave {/hdmi/axi_vdma_0/S_AXI_LITE} intc_ip {New AXI Interconnect} master_apm {0}}  [get_bd_intf_pins hdmi/axi_vdma_0/S_AXI_LITE]
#</hdmi/axi_vdma_0/S_AXI_LITE/Reg> is being mapped into </processing_system7_0/Data> at <0x43000000 [ 64K ]>
apply_bd_automation -rule xilinx.com:bd_rule:clkrst -config {Clk "/processing_system7_0/FCLK_CLK0 (100 MHz)" }  [get_bd_pins hdmi/axi_vdma_0/m_axis_mm2s_aclk]
apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config { Clk_master {Auto} Clk_slave {Auto} Clk_xbar {Auto} Master {/processing_system7_0/M_AXI_GP0} Slave {/hdmi/v_tc_0/ctrl} intc_ip {New AXI Interconnect} master_apm {0}}  [get_bd_intf_pins hdmi/v_tc_0/ctrl]
#</hdmi/v_tc_0/ctrl/Reg> is being mapped into </processing_system7_0/Data> at <0x43C10000 [ 64K ]>
apply_bd_automation -rule xilinx.com:bd_rule:clkrst -config {Clk "/processing_system7_0/FCLK_CLK0 (100 MHz)" }  [get_bd_pins hdmi/v_axi4s_vid_out_0/aclk]
#INFO: [BD 5-455] Automation on '/hdmi/axis_subset_converter_0/aclk' will not be run, since it is obsolete due to previously run automations
endgroup
#SET HDMI OUT RESOULUTION TO 640x480
startgroup
set_property -dict [list CONFIG.VIDEO_MODE {640x480p} CONFIG.GEN_F0_VSYNC_VSTART {489} CONFIG.GEN_F1_VSYNC_VSTART {489} CONFIG.GEN_HACTIVE_SIZE {640} CONFIG.GEN_HSYNC_END {752} CONFIG.GEN_HFRAME_SIZE {800} CONFIG.GEN_F0_VSYNC_HSTART {640} CONFIG.GEN_F1_VSYNC_HSTART {640} CONFIG.GEN_F0_VSYNC_HEND {640} CONFIG.GEN_F1_VSYNC_HEND {640} CONFIG.GEN_F0_VFRAME_SIZE {525} CONFIG.GEN_F1_VFRAME_SIZE {525} CONFIG.GEN_F0_VSYNC_VEND {491} CONFIG.GEN_F1_VSYNC_VEND {491} CONFIG.GEN_F0_VBLANK_HEND {640} CONFIG.GEN_F1_VBLANK_HEND {640} CONFIG.GEN_HSYNC_START {656} CONFIG.GEN_VACTIVE_SIZE {480} CONFIG.GEN_F0_VBLANK_HSTART {640} CONFIG.GEN_F1_VBLANK_HSTART {640}] [get_bd_cells hdmi/v_tc_0]
endgroup

#add AXI_ARPS_IP, output port and connect them
startgroup
create_bd_cell -type ip -vlnv FTN:user:AXI_ARPS_IP:1.0 AXI_ARPS_IP_0
endgroup

# BRAM_CURR
startgroup
create_bd_cell -type ip -vlnv xilinx.com:ip:blk_mem_gen:8.4 blk_mem_gen_0
endgroup
set_property -dict [list CONFIG.Memory_Type {True_Dual_Port_RAM} CONFIG.Enable_B {Use_ENB_Pin} CONFIG.Use_RSTB_Pin {true} CONFIG.Port_B_Clock {100} CONFIG.Port_B_Write_Rate {50} CONFIG.Port_B_Enable_Rate {100}] [get_bd_cells blk_mem_gen_0]
set_property name BRAM_CURR [get_bd_cells blk_mem_gen_0]

#BRAM_REF
startgroup
create_bd_cell -type ip -vlnv xilinx.com:ip:blk_mem_gen:8.4 blk_mem_gen_0
endgroup
set_property -dict [list CONFIG.Memory_Type {True_Dual_Port_RAM} CONFIG.Enable_B {Use_ENB_Pin} CONFIG.Use_RSTB_Pin {true} CONFIG.Port_B_Clock {100} CONFIG.Port_B_Write_Rate {50} CONFIG.Port_B_Enable_Rate {100}] [get_bd_cells blk_mem_gen_0]
set_property name BRAM_REF [get_bd_cells blk_mem_gen_0]

#BRAM_MV
startgroup
create_bd_cell -type ip -vlnv xilinx.com:ip:blk_mem_gen:8.4 blk_mem_gen_0
endgroup
set_property -dict [list CONFIG.Memory_Type {True_Dual_Port_RAM} CONFIG.Enable_B {Use_ENB_Pin} CONFIG.Use_RSTB_Pin {true} CONFIG.Port_B_Clock {100} CONFIG.Port_B_Write_Rate {50} CONFIG.Port_B_Enable_Rate {100}] [get_bd_cells blk_mem_gen_0]
set_property name BRAM_MV [get_bd_cells blk_mem_gen_0]

#BRAM_CTRL_0
startgroup
create_bd_cell -type ip -vlnv xilinx.com:ip:axi_bram_ctrl:4.1 axi_bram_ctrl_0
endgroup
set_property -dict [list CONFIG.SINGLE_PORT_BRAM {1}] [get_bd_cells axi_bram_ctrl_0]

#BRAM_CTRL_1
startgroup
create_bd_cell -type ip -vlnv xilinx.com:ip:axi_bram_ctrl:4.1 axi_bram_ctrl_1
endgroup
set_property -dict [list CONFIG.SINGLE_PORT_BRAM {1}] [get_bd_cells axi_bram_ctrl_1]

#BRAM_CTRL_2
startgroup
create_bd_cell -type ip -vlnv xilinx.com:ip:axi_bram_ctrl:4.1 axi_bram_ctrl_2
endgroup
set_property -dict [list CONFIG.SINGLE_PORT_BRAM {1}] [get_bd_cells axi_bram_ctrl_2]

#connect BRAM_interfaces
connect_bd_intf_net [get_bd_intf_pins AXI_ARPS_IP_0/BRAM_CURR] [get_bd_intf_pins BRAM_CURR/BRAM_PORTB]
connect_bd_intf_net [get_bd_intf_pins AXI_ARPS_IP_0/BRAM_MV] [get_bd_intf_pins BRAM_MV/BRAM_PORTB]
connect_bd_intf_net [get_bd_intf_pins AXI_ARPS_IP_0/BRAM_REF] [get_bd_intf_pins BRAM_REF/BRAM_PORTB]

connect_bd_intf_net [get_bd_intf_pins axi_bram_ctrl_0/BRAM_PORTA] [get_bd_intf_pins BRAM_CURR/BRAM_PORTA]
connect_bd_intf_net [get_bd_intf_pins axi_bram_ctrl_1/BRAM_PORTA] [get_bd_intf_pins BRAM_REF/BRAM_PORTA]
connect_bd_intf_net [get_bd_intf_pins axi_bram_ctrl_2/BRAM_PORTA] [get_bd_intf_pins BRAM_MV/BRAM_PORTA]
connect_bd_net [get_bd_pins processing_system7_0/IRQ_F2P] [get_bd_pins AXI_ARPS_IP_0/interrupt]

#run connection automation
update_compile_order -fileset sources_1
startgroup
apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config { Clk_master {Auto} Clk_slave {Auto} Clk_xbar {Auto} Master {/processing_system7_0/M_AXI_GP0} Slave {/AXI_ARPS_IP_0/s00_axi} intc_ip {New AXI Interconnect} master_apm {0}}  [get_bd_intf_pins AXI_ARPS_IP_0/s00_axi]
#INFO: [Ipptcl 7-1463] No Compatible Board Interface found. Board Tab not created in customize GUI
#</AXI_ARPS_IP_0/s00_axi/reg0> is being mapped into </processing_system7_0/Data> at <0x43C00000 [ 64K ]>
apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config { Clk_master {Auto} Clk_slave {Auto} Clk_xbar {Auto} Master {/processing_system7_0/M_AXI_GP0} Slave {/axi_bram_ctrl_0/S_AXI} intc_ip {Auto} master_apm {0}}  [get_bd_intf_pins axi_bram_ctrl_0/S_AXI]
#</axi_bram_ctrl_0/S_AXI/Mem0> is being mapped into </processing_system7_0/Data> at <0x40000000 [ 8K ]>
apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config { Clk_master {Auto} Clk_slave {Auto} Clk_xbar {Auto} Master {/processing_system7_0/M_AXI_GP0} Slave {/axi_bram_ctrl_1/S_AXI} intc_ip {Auto} master_apm {0}}  [get_bd_intf_pins axi_bram_ctrl_1/S_AXI]
#</axi_bram_ctrl_1/S_AXI/Mem0> is being mapped into </processing_system7_0/Data> at <0x42000000 [ 8K ]>
apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config { Clk_master {Auto} Clk_slave {Auto} Clk_xbar {Auto} Master {/processing_system7_0/M_AXI_GP0} Slave {/axi_bram_ctrl_2/S_AXI} intc_ip {Auto} master_apm {0}}  [get_bd_intf_pins axi_bram_ctrl_2/S_AXI]
#</axi_bram_ctrl_2/S_AXI/Mem0> is being mapped into </processing_system7_0/Data> at <0x44000000 [ 8K ]>
endgroup

startgroup
set_property range 64K [get_bd_addr_segs {processing_system7_0/Data/SEG_axi_bram_ctrl_0_Mem0}]
set_property range 64K [get_bd_addr_segs {processing_system7_0/Data/SEG_axi_bram_ctrl_1_Mem0}]
set_property range 4K [get_bd_addr_segs {processing_system7_0/Data/SEG_axi_bram_ctrl_2_Mem0}]
endgroup

validate_bd_design

#Creating hdl wrapper
make_wrapper -files [get_files $resultDir/arps.srcs/sources_1/bd/arps_bl_dsg/arps_bl_dsg.bd] -top

add_files -norecurse $resultDir/arps.srcs/sources_1/bd/arps_bl_dsg/hdl/arps_bl_dsg_wrapper.vhd
update_compile_order -fileset sources_1

#running synthesis and implementation
launch_runs impl_1 -to_step write_bitstream -jobs 2

#exporting hardware
wait_on_run impl_1
update_compile_order -fileset sources_1
file mkdir $resultDir/arps.sdk
file copy -force $resultDir/arps.runs/impl_1/arps_bl_dsg_wrapper.sysdef $resultDir/arps.sdk/arps_bl_dsg_wrapper.hdf