####################################################################################################################################################
The main IP (ARPS) searches for motion vector (MV) between current and reference frame.
When the MV are found, in SW we reconstruct the current frame, based on referece frame and MV.
Then, when it's reconstructed, we create a new image, which is a absolut difference between original current frame and reconstructed current frame. 
This image is shown on monitor.
###################################################################################################################################################
The TCL script is tested in Vivado 2018.3 and it works on Windows 7 and Windows 10.
It's necessary to download board files for Zybo Z7-10 from Digilet site.
To run TCL script type in Vivado TCL console "source <location of Master.tcl>" . 
For testing use SDK code located in /els/project/PSDS/sdk/sdk_arps_with_hdmi.
Resoultion of HDMI IP core is set to 640x480.
For logs connect to serial port, set the COM or ttyUSB port and BAUDRATE to 115200.
Frames used for testing are 90 and 91 from "golf player" video.
Use HDMI cabel and one side connect to HDMI_TX port of Zybo Z7-10 and other side to HDMI input port of monitor.

This HDMI IP core is used from: https://github.com/RTSYork/zybo-z7-hdmi
