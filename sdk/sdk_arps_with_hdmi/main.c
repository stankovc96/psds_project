/**
 * Zybo Z7-10  Zynq 7000 SoC
 * ARPS (Adaptive_Rood_Patter_Search) - Block Matching - Motion Estimation - Video Motion Detection System
 * Stefan Stankovic EE17/2015
 * University of Novi Sad
 * Faculty of Technical Science
 * Novi Sad
 *
 */

#include <stdio.h>
#include "sleep.h"
#include "xil_types.h"
#include "xil_cache.h"
#include "xparameters.h"
#include "zybo_z7_hdmi/display_ctrl.h"
#include "sample90.h"
#include "sample91.h"
#include "sleep.h"
#include "stdlib.h"
#include "math.h"
#include "xscugic.h"
#include <stdint.h>

/*BASE ADDRESS */
#define AXIL_BASE_ADDR    XPAR_AXI_ARPS_IP_0_BASEADDR
#define BRAM_CURR_CTRL XPAR_AXI_BRAM_CTRL_0_S_AXI_BASEADDR
#define BRAM_REF_CTRL XPAR_AXI_BRAM_CTRL_1_S_AXI_BASEADDR
#define BRAM_MV_CTRL XPAR_AXI_BRAM_CTRL_2_S_AXI_BASEADDR
/*INTERRUPT*/
#define ARPS_IRQID XPAR_FABRIC_AXI_ARPS_IP_0_INTERRUPT_INTR

/*------------------------------------*/
            /*REGISTERS*/
/*------------------------------------*/
#define START_ADDR AXIL_BASE_ADDR
#define READY_ADDR AXIL_BASE_ADDR+4

/*DEFINE PARAMETERS*/
#define ROW_SIZE 256
#define COL_SIZE 256
#define MB_SIZE 16
#define P_SIZE 7
#define BRAM_IMG_LOC (ROW_SIZE*COL_SIZE)/4
//------------------------------------/

/*DEFINE PARAMETERS*/
#define ROW_SIZE 256
#define COL_SIZE 256
#define MB_SIZE 16
#define P_SIZE 7


        /*HDMI DEFINE RESOLUTION*/
/* Frame size (based on 640x480 resolution, 32 bits per pixel)*/
#define MAX_FRAME (640*480)
#define FRAME_STRIDE (640*4)

            /*INTERRUPT*/
/*----------------------------------*/
XScuGic Intc_ins;
XScuGic_Config Intc_cfg;
static volatile u32 interrupt_flag;
void init_IRQ();
void ARPS_ISR(void *CallbackRef);
/*----------------------------------*/


            /*HDMI FUNC*/
/*----------------------------------*/
DisplayCtrl dispCtrl; // Display driver struct
u32 frameBuf[DISPLAY_NUM_FRAMES][MAX_FRAME] __attribute__((aligned(0x20))); // Frame buffers for video data
void *pFrames[DISPLAY_NUM_FRAMES]; // Array of pointers to the frame buffers
/*----------------------------------*/

            /*FUNC*/
/*----------------------------------*/
u32 conv32to4signed(u32);
/*----------------------------------*/

u32 imageComp[65536];
int32_t mv[512];

int main(void) {
	//u32 i=0,j=0;
	/*INTERRUPT*/
	/*------------------------------------------------------------*/
	interrupt_flag=0;
	xil_printf("Interrupt=%d \n",interrupt_flag);
    init_IRQ();

	/*------------------------------------------------------------*/

	// Initialise an array of pointers to the 2 frame buffers

    uint32_t data=0;
    //init_platform();
    xil_printf("Writing data in BRAM_CURR and BRAM_REF... \n");
    for(int i=0;i<BRAM_IMG_LOC;i++){
    	data=(pix91[4*i]<<24)+(pix91[4*i+1]<<16)+(pix91[4*i+2]<<8)+pix91[4*i+3];
        Xil_Out32(BRAM_CURR_CTRL+(i*4),data);
    	data=(pix90[4*i]<<24)+(pix90[4*i+1]<<16)+(pix90[4*i+2]<<8)+pix90[4*i+3];
        Xil_Out32(BRAM_REF_CTRL+(i*4),data);
    }
    xil_printf("Done writing. \n\n");

    while(Xil_In32(READY_ADDR)==0){}

    xil_printf("Ready = 1\n");

    /*Start = 1*/
    Xil_Out32(START_ADDR, 1);
    xil_printf("Start = 1 \n");

    /*Start = 0*/
    Xil_Out32(START_ADDR, 0);//start
    xil_printf("Start = 0 \n");

    while(interrupt_flag==0){

    }

    xil_printf("READING DATA FROM MV BRAM \n");
	//READ MV FROM BRAM

	for(int i=0;i<512;i++){
        //Xil_Out32(mv_addr_i,i);
        mv[i]=Xil_In32(BRAM_MV_CTRL+i*4);
	    //xil_printf("mv[%d]=%d\n",i,conv32to4signed(Xil_In32(mv_data_o)));
	}
	//***************************
	unsigned char imgRec[65536];
	int mbCount=0;
	int dx,dy;
	int rr,cc;

    /*Reconstruction of Curr Img*/
	for (int i=0;i<(ROW_SIZE-MB_SIZE+1);i+=MB_SIZE){
        for (int j=0;j<(COL_SIZE-MB_SIZE+1);j+=MB_SIZE){
            dx=j+mv[mbCount];//col
	    	dy=i+mv[mbCount+1];//row
	    	mbCount=mbCount+2;
	    	rr=i-1;
	    	cc=j-1;
	    	for(int r=dy;r<dy+MB_SIZE;r++){
                rr++;
	    		cc=j-1;
	    		for(int c=dx;c<dx+MB_SIZE;c++){
                    cc++;
	    			if(r<0 || c<0 || r>=ROW_SIZE || c>=COL_SIZE)
                        continue;
	    			imgRec[256*rr+cc] = pix90[256*r+c];
	    		}
	    	}
	    }
	}
	unsigned char temp[65536];
	for(int ii=0;ii<65536;ii++){
		temp[ii]=(unsigned char)(abs((int)(imgRec[ii])-(int)(pix91[ii])));
	}
    /********************************************************************************/
                                /*SEND IMAGE VIA HDMI*/
    /********************************************************************************/
	int i;
	for (i = 0; i < DISPLAY_NUM_FRAMES; i++)
		pFrames[i] = frameBuf[i];

	// Initialise the display controller
	DisplayInitialize(&dispCtrl, XPAR_AXIVDMA_0_DEVICE_ID, XPAR_VTC_0_DEVICE_ID, XPAR_HDMI_AXI_DYNCLK_0_BASEADDR, pFrames, FRAME_STRIDE);

	// Use first frame buffer (of two)
	DisplayChangeFrame(&dispCtrl, 0);

	// Set the display resolution
	DisplaySetMode(&dispCtrl, &VMODE_640x480);

	// Enable video output
	DisplayStart(&dispCtrl);

	printf("\n\r");
	printf("HDMI output enabled\n\r");
	printf("Current Resolution: %s\n\r", dispCtrl.vMode.label);
	printf("Pixel Clock Frequency: %.3fMHz\n\r", dispCtrl.pxlFreq);
	printf("Drawing gradient pattern to screen...\n\r");

	// Get parameters from display controller struct
	int x, y;
	u32 stride = dispCtrl.stride / 4;
	u32 width = dispCtrl.vMode.width;
	u32 height = dispCtrl.vMode.height;
	u32 *frame = (u32 *)dispCtrl.framePtr[dispCtrl.curFrame];
	u32 red, green, blue;
	u32 ii=0;

	xil_printf("height=%d\n",height);
	xil_printf("width=%d\n",width);
	//unsigned char temp;
	// Fill the screen with a nice gradient pattern
	for (y = 0; y < height; y++) {//480
		for (x = 0; x < width; x++) {//640
			if(y>255 || x>255){
				green = 0xff;
				blue = 0xff;
				red = 0x00;
			}
			else{

				green=temp[ii];
				blue=temp[ii];
				red=temp[ii];
				ii=ii+1;

			}

			frame[y*stride + x] = (red << BIT_DISPLAY_RED) | (green << BIT_DISPLAY_GREEN) | (blue << BIT_DISPLAY_BLUE);
		}
	}
	ii=0;
	// Flush the cache, so the Video DMA core can pick up our frame buffer changes.
	// Flushing the entire cache (rather than a subset of cache lines) makes sense as our buffer is so big
	Xil_DCacheFlush();

	printf("Done.\n\r");

	return 0;
}
/********************************************************************************/
void ARPS_ISR(void *CallbackRef)
{
	interrupt_flag=1;
	xil_printf("Interrupt=%d \n",interrupt_flag);
}
/********************************************************************************/
u32 conv32to4signed(u32 data){
	u32 d;
	if(data>7){
		d=data-16;
	}
	else{
		d=data;
	}
	return d;
}
/********************************************************************************/
void init_IRQ()
{
	XScuGic *Intc_ins_p= &Intc_ins;
	XScuGic_Config *Intc_cfg_p= &Intc_cfg;
	Intc_cfg_p = XScuGic_LookupConfig(0);
	XScuGic_CfgInitialize(Intc_ins_p, Intc_cfg_p,Intc_cfg_p->CpuBaseAddress);
	XScuGic_SetPriorityTriggerType(Intc_ins_p, 61,0x00, 0x3);
	Xil_ExceptionInit();
	Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_IRQ_INT,
					(Xil_ExceptionHandler)XScuGic_InterruptHandler,
					Intc_ins_p);
	XScuGic_Connect(Intc_ins_p,ARPS_IRQID,
					 (Xil_ExceptionHandler)ARPS_ISR,
					 (void *)&Intc_ins_p);
	XScuGic_Enable(Intc_ins_p,ARPS_IRQID);
	Xil_ExceptionEnable();
}
/********************************************************************************/
