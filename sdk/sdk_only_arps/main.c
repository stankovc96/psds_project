
#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"
#include "sample90.h" //golfer frame 90 (reference)
#include "sample91.h" //golfer frame 91 (current)
#include "s90s91.h"
#include "xparameters.h"
#include "xil_io.h"
#include "xscugic.h"
#include "xil_cache.h"

#define AXIL_ARPS XPAR_AXI_ARPS_IP_0_BASEADDR
#define BRAM_CURR_CTRL XPAR_AXI_BRAM_CTRL_0_S_AXI_BASEADDR
#define BRAM_REF_CTRL XPAR_AXI_BRAM_CTRL_1_S_AXI_BASEADDR
#define BRAM_MV_CTRL XPAR_AXI_BRAM_CTRL_2_S_AXI_BASEADDR
#define ARPS_IRQID XPAR_FABRIC_AXI_ARPS_IP_0_INTERRUPT_INTR
#define START_ADDR AXIL_ARPS
#define READY_ADDR AXIL_ARPS+4

/*DEFINE PARAMETERS*/
#define ROW_SIZE 256
#define COL_SIZE 256
#define MB_SIZE 16
#define P_SIZE 7
#define BRAM_IMG_LOC (ROW_SIZE*COL_SIZE)/4
            /*INTERRUPT*/
/*----------------------------------*/
XScuGic Intc_ins;
XScuGic_Config Intc_cfg;
static volatile u32 interrupt_flag;
void init_IRQ();
void ARPS_ISR(void *CallbackRef);
/*----------------------------------*/

int main()
{
    init_platform();
    Xil_DCacheDisable();
    Xil_ICacheDisable();
    u32 i=0,j=0;
    //u32 mv[512];
    /*INTERRUPT*/
    /*------------------------------------------------------------*/
    interrupt_flag=0;
    xil_printf("Interrupt = %d \n",interrupt_flag);
    init_IRQ();

    /*WRITING DATA IN BRAM_CURR AND BRAM_REF*/
    /*------------------------------------------------------------*/
    xil_printf("Writing data in BRAM_CURR and BRAM_REF... \n");
    for(i=0;i<BRAM_IMG_LOC;i++){
    	//xil_printf("addr[%d]=%x, data=[%d]=%x\n",j,bram_base+(j*4),j,pix91[j]);
    	Xil_Out32(BRAM_CURR_CTRL+(i*4),pix91[i]);
    	Xil_Out32(BRAM_REF_CTRL+(i*4),pix90[i]);
    }
    Xil_DCacheFlush();
    xil_printf("Done writing. \n\n");

    while(Xil_In32(READY_ADDR)==0){

    }
    xil_printf("Ready = 1\n");

    /*Start = 1*/
    Xil_Out32(START_ADDR, 1);
    xil_printf("Start = 1 \n");

    /*Start = 0*/
    Xil_Out32(START_ADDR, 0);//start
    xil_printf("Start = 0 \n");

    //wait for interrupt
    while(interrupt_flag==0){

    }
    xil_printf("Interrupt = 1\n");
    int fail=0;
    xil_printf("Reading data from BRAM_MV... \n\n");
    for(j=0;j<512;j++){
        //xil_printf("d[%d]=%d\n",j,Xil_In32(BRAM_MV_CTRL+j*4));

        if(s90s91[j]!=Xil_In32(BRAM_MV_CTRL+j*4)){
    		fail=1;
    		xil_printf("ERROR: j=%d\n",j);
    	}

    }
    Xil_DCacheFlush();
    //----------------------------------------------------------------
    xil_printf("Verification...\n");
    if(fail!=1){
    	xil_printf("MV match!\n");
    }
    else{
    	xil_printf("MV don't match!\n");
    }
    xil_printf("Done reading. \n\n");


    cleanup_platform();
    return 0;
}
/********************************************************************************/
void ARPS_ISR(void *CallbackRef)
{
	interrupt_flag=1;
	//xil_printf("Interrupt = %d \n",interrupt_flag);
}
/********************************************************************************/
void init_IRQ()
{
	XScuGic *Intc_ins_p= &Intc_ins;
	XScuGic_Config *Intc_cfg_p= &Intc_cfg;
	Intc_cfg_p = XScuGic_LookupConfig(0);
	XScuGic_CfgInitialize(Intc_ins_p, Intc_cfg_p,Intc_cfg_p->CpuBaseAddress);
	XScuGic_SetPriorityTriggerType(Intc_ins_p, 61,0x00, 0x3);
	Xil_ExceptionInit();
	Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_IRQ_INT,
					(Xil_ExceptionHandler)XScuGic_InterruptHandler,
					Intc_ins_p);
	XScuGic_Connect(Intc_ins_p,ARPS_IRQID,
					 (Xil_ExceptionHandler)ARPS_ISR,
					 (void *)&Intc_ins_p);
	XScuGic_Enable(Intc_ins_p,ARPS_IRQID);
	Xil_ExceptionEnable();
}
/********************************************************************************/

