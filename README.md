## APRS - Adaptive Rood Pattern Search algorithm:

### Used tools:

​	1.Xilinx Vivado 2018.3
​	2.Xilinx SDK 2018.3

### Resouces and freq

- Freq = 100 MHz
- LUTs   : 2332

- FF-s   : 2502
- BRAM-s : 34

### Update: 16.08.2020.
Initialy sad_block.vhd had 4 states: idle, s1, s2 and s3.
In state s2 it was claculating addresses for BRAM and in state s3 it was claculating absolut error based on input data.
After s3 was finished, it changed the state to s2 and had a one spare cycle for calculating absolut error.
Now the sad_block.vhd has 3 states: idle, s1 and s2. 
In s1 when starting FSM claculates initial address based on inputs and then in state s2 calculates absolut error.
Also in s2, calculates the next address for BRAM and then the next state is s2 unitl the i_reg reaches it max value.

In sad_block.vhd are added 2 output ports for enabiling BRAM memory inteface. Initialy they are alwayes on logic 1 in bram_if.vhd.