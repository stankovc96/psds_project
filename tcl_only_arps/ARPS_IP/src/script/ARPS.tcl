variable dispScriptFile [file normalize [info script]]

proc getScriptDirectory {} {
    variable dispScriptFile
    set scriptFolder [file dirname $dispScriptFile]
    return $scriptFolder
}

set sdir [getScriptDirectory]
cd [getScriptDirectory]

# KORAK#1: Definisanje direktorijuma u kojima ce biti smesteni projekat i konfiguracioni fajl
set resultDir ..\/..\/result\/ARPS
file mkdir $resultDir
create_project pkg_arps_ip ..\/..\/result\/ARPS -part xc7z010clg400-1 -force
set_property target_language VHDL [current_project]
set_property board_part digilentinc.com:zybo-z7-10:part0:1.0 [current_project]

# KORAK#2: Ukljucivanje svih izvornih fajlova u projekat
add_files -norecurse ..\/hdl\/utils_pkg.vhd
add_files -norecurse ..\/hdl\/sad_block.vhd
add_files -norecurse ..\/hdl\/control_block.vhd
add_files -norecurse ..\/hdl\/bram_if.vhd
add_files -norecurse ..\/hdl\/arps_ip.vhd
add_files -norecurse ..\/hdl\/AXI_ARPS_IP_v1_0_S00_AXI.vhd
add_files -norecurse ..\/hdl\/AXI_ARPS_IP_v1_0.vhd

update_compile_order -fileset sources_1

# KORAK#3: Pokretanje procesa sinteze
launch_runs synth_1
wait_on_run synth_1
puts "*****************************************************"
puts "* Sinteza zavrsena! *"
puts "*****************************************************"

# KORAK#4: Pakovanje Jezgra
update_compile_order -fileset sources_1
ipx::package_project -root_dir ..\/..\/ -vendor xilinx.com -library user -taxonomy /UserIP -force

set_property vendor FTN [ipx::current_core]
set_property name AXI_ARPS_IP [ipx::current_core]
set_property display_name AXI_ARPS_IP_V1_0 [ipx::current_core]
set_property description { Searches for Motion Vectors} [ipx::current_core]
set_property company_url http://www.ftn.uns.ac.rs [ipx::current_core]
set_property vendor_display_name FTN [ipx::current_core]
set_property taxonomy {/UserIP} [ipx::current_core]
set_property supported_families {zynq Production} [ipx::current_core]

#BRAM_MV bus
ipx::add_bus_interface BRAM_MV [ipx::current_core]
set_property abstraction_type_vlnv xilinx.com:interface:bram_rtl:1.0 [ipx::get_bus_interfaces BRAM_MV -of_objects [ipx::current_core]]
set_property bus_type_vlnv xilinx.com:interface:bram:1.0 [ipx::get_bus_interfaces BRAM_MV -of_objects [ipx::current_core]]
set_property interface_mode master [ipx::get_bus_interfaces BRAM_MV -of_objects [ipx::current_core]]
ipx::add_port_map DIN [ipx::get_bus_interfaces BRAM_MV -of_objects [ipx::current_core]]
set_property physical_name dinb_mv_o [ipx::get_port_maps DIN -of_objects [ipx::get_bus_interfaces BRAM_MV -of_objects [ipx::current_core]]]
ipx::add_port_map EN [ipx::get_bus_interfaces BRAM_MV -of_objects [ipx::current_core]]
set_property physical_name enb_mv_o [ipx::get_port_maps EN -of_objects [ipx::get_bus_interfaces BRAM_MV -of_objects [ipx::current_core]]]
ipx::add_port_map RST [ipx::get_bus_interfaces BRAM_MV -of_objects [ipx::current_core]]
set_property physical_name rstb_mv_o [ipx::get_port_maps RST -of_objects [ipx::get_bus_interfaces BRAM_MV -of_objects [ipx::current_core]]]
ipx::add_port_map CLK [ipx::get_bus_interfaces BRAM_MV -of_objects [ipx::current_core]]
set_property physical_name clk_mv_o [ipx::get_port_maps CLK -of_objects [ipx::get_bus_interfaces BRAM_MV -of_objects [ipx::current_core]]]
ipx::add_port_map WE [ipx::get_bus_interfaces BRAM_MV -of_objects [ipx::current_core]]
set_property physical_name web_mv_o [ipx::get_port_maps WE -of_objects [ipx::get_bus_interfaces BRAM_MV -of_objects [ipx::current_core]]]
ipx::add_port_map ADDR [ipx::get_bus_interfaces BRAM_MV -of_objects [ipx::current_core]]
set_property physical_name addrb_mv_o [ipx::get_port_maps ADDR -of_objects [ipx::get_bus_interfaces BRAM_MV -of_objects [ipx::current_core]]]
set_property display_name BRAM_MV [ipx::get_bus_interfaces BRAM_MV -of_objects [ipx::current_core]]
set_property description BRAM_MV [ipx::get_bus_interfaces BRAM_MV -of_objects [ipx::current_core]]

#BRAM_CURR bus
ipx::add_bus_interface BRAM_CURR [ipx::current_core]
set_property abstraction_type_vlnv xilinx.com:interface:bram_rtl:1.0 [ipx::get_bus_interfaces BRAM_CURR -of_objects [ipx::current_core]]
set_property bus_type_vlnv xilinx.com:interface:bram:1.0 [ipx::get_bus_interfaces BRAM_CURR -of_objects [ipx::current_core]]
set_property interface_mode master [ipx::get_bus_interfaces BRAM_CURR -of_objects [ipx::current_core]]
set_property display_name BRAM_CURR [ipx::get_bus_interfaces BRAM_CURR -of_objects [ipx::current_core]]
set_property description BRAM_CURR [ipx::get_bus_interfaces BRAM_CURR -of_objects [ipx::current_core]]
ipx::add_port_map EN [ipx::get_bus_interfaces BRAM_CURR -of_objects [ipx::current_core]]
set_property physical_name enb_curr_o [ipx::get_port_maps EN -of_objects [ipx::get_bus_interfaces BRAM_CURR -of_objects [ipx::current_core]]]
ipx::add_port_map DOUT [ipx::get_bus_interfaces BRAM_CURR -of_objects [ipx::current_core]]
set_property physical_name doutb_curr_i [ipx::get_port_maps DOUT -of_objects [ipx::get_bus_interfaces BRAM_CURR -of_objects [ipx::current_core]]]
ipx::add_port_map RST [ipx::get_bus_interfaces BRAM_CURR -of_objects [ipx::current_core]]
set_property physical_name rstb_curr_o [ipx::get_port_maps RST -of_objects [ipx::get_bus_interfaces BRAM_CURR -of_objects [ipx::current_core]]]
ipx::add_port_map CLK [ipx::get_bus_interfaces BRAM_CURR -of_objects [ipx::current_core]]
set_property physical_name clk_curr_o [ipx::get_port_maps CLK -of_objects [ipx::get_bus_interfaces BRAM_CURR -of_objects [ipx::current_core]]]
ipx::add_port_map WE [ipx::get_bus_interfaces BRAM_CURR -of_objects [ipx::current_core]]
set_property physical_name web_curr_o [ipx::get_port_maps WE -of_objects [ipx::get_bus_interfaces BRAM_CURR -of_objects [ipx::current_core]]]
ipx::add_port_map ADDR [ipx::get_bus_interfaces BRAM_CURR -of_objects [ipx::current_core]]
set_property physical_name addrb_curr_o [ipx::get_port_maps ADDR -of_objects [ipx::get_bus_interfaces BRAM_CURR -of_objects [ipx::current_core]]]

#BRAM_REF bus
ipx::add_bus_interface BRAM_REF [ipx::current_core]
set_property abstraction_type_vlnv xilinx.com:interface:bram_rtl:1.0 [ipx::get_bus_interfaces BRAM_REF -of_objects [ipx::current_core]]
set_property bus_type_vlnv xilinx.com:interface:bram:1.0 [ipx::get_bus_interfaces BRAM_REF -of_objects [ipx::current_core]]
set_property interface_mode master [ipx::get_bus_interfaces BRAM_REF -of_objects [ipx::current_core]]
set_property display_name BRAM_REF [ipx::get_bus_interfaces BRAM_REF -of_objects [ipx::current_core]]
set_property description BRAM_REF [ipx::get_bus_interfaces BRAM_REF -of_objects [ipx::current_core]]
ipx::add_port_map EN [ipx::get_bus_interfaces BRAM_REF -of_objects [ipx::current_core]]
set_property physical_name enb_ref_o [ipx::get_port_maps EN -of_objects [ipx::get_bus_interfaces BRAM_REF -of_objects [ipx::current_core]]]
ipx::add_port_map DOUT [ipx::get_bus_interfaces BRAM_REF -of_objects [ipx::current_core]]
set_property physical_name doutb_ref_i [ipx::get_port_maps DOUT -of_objects [ipx::get_bus_interfaces BRAM_REF -of_objects [ipx::current_core]]]
ipx::add_port_map RST [ipx::get_bus_interfaces BRAM_REF -of_objects [ipx::current_core]]
set_property physical_name rstb_ref_o [ipx::get_port_maps RST -of_objects [ipx::get_bus_interfaces BRAM_REF -of_objects [ipx::current_core]]]
ipx::add_port_map CLK [ipx::get_bus_interfaces BRAM_REF -of_objects [ipx::current_core]]
set_property physical_name clk_ref_o [ipx::get_port_maps CLK -of_objects [ipx::get_bus_interfaces BRAM_REF -of_objects [ipx::current_core]]]
ipx::add_port_map WE [ipx::get_bus_interfaces BRAM_REF -of_objects [ipx::current_core]]
set_property physical_name web_ref_o [ipx::get_port_maps WE -of_objects [ipx::get_bus_interfaces BRAM_REF -of_objects [ipx::current_core]]]
ipx::add_port_map ADDR [ipx::get_bus_interfaces BRAM_REF -of_objects [ipx::current_core]]
set_property physical_name addrb_ref_o [ipx::get_port_maps ADDR -of_objects [ipx::get_bus_interfaces BRAM_REF -of_objects [ipx::current_core]]]




ipx::update_checksums [ipx::current_core]
ipx::save_core [ipx::current_core]
set_property core_revision 2 [ipx::current_core]
ipx::create_xgui_files [ipx::current_core]
ipx::update_checksums [ipx::current_core]
ipx::save_core [ipx::current_core]
set_property  ip_repo_paths ..\/..\/ [current_project]
update_ip_catalog
ipx::check_integrity -quiet [ipx::current_core]
ipx::archive_core ..\/..\/AXI_ARPS_IP_V1_0.zip [ipx::current_core]
close_project