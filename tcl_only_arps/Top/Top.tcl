#process for getting script file directory
variable dispScriptFile [file normalize [info script]]
proc getScriptDirectory {} {
    variable dispScriptFile
    set scriptFolder [file dirname $dispScriptFile]
    return $scriptFolder
}

#change working directory to script file directory
cd [getScriptDirectory]
#set result directory
set resultDir .\/result
#set ip_repo_path to script dir
set ip_repo_path [getScriptDirectory]\/..\/
#redifine resultDir HERE if needed
#set resutDir C:\/User\/result

file mkdir $resultDir

# CONNECT SYSTEM
create_project arps $resultDir  -part xc7z010clg400-1 -force
set_property board_part digilentinc.com:zybo-z7-10:part0:1.0 [current_project]
set_property target_language VHDL [current_project]
create_bd_design "arps_bl_dsg"
update_compile_order -fileset sources_1

#add ip-s to main repo
set_property  ip_repo_paths  $ip_repo_path [current_project]
update_ip_catalog

#add and configure zynq
startgroup
create_bd_cell -type ip -vlnv xilinx.com:ip:processing_system7:5.5 processing_system7_0
endgroup

set_property -dict [list CONFIG.PCW_FPGA0_PERIPHERAL_FREQMHZ {100} CONFIG.PCW_USE_FABRIC_INTERRUPT {1} CONFIG.PCW_IRQ_F2P_INTR {1}] [get_bd_cells processing_system7_0]
startgroup

apply_bd_automation -rule xilinx.com:bd_rule:processing_system7 -config {make_external "FIXED_IO, DDR" apply_board_preset "1" Master "Disable" Slave "Disable" }  [get_bd_cells processing_system7_0]
endgroup


#add AXI_ARPS_IP, output port and connect them
startgroup
create_bd_cell -type ip -vlnv FTN:user:AXI_ARPS_IP:1.0 AXI_ARPS_IP_0
endgroup

# BRAM_CURR
startgroup
create_bd_cell -type ip -vlnv xilinx.com:ip:blk_mem_gen:8.4 blk_mem_gen_0
endgroup
set_property -dict [list CONFIG.Memory_Type {True_Dual_Port_RAM} CONFIG.Enable_B {Use_ENB_Pin} CONFIG.Use_RSTB_Pin {true} CONFIG.Port_B_Clock {100} CONFIG.Port_B_Write_Rate {50} CONFIG.Port_B_Enable_Rate {100}] [get_bd_cells blk_mem_gen_0]
set_property name BRAM_CURR [get_bd_cells blk_mem_gen_0]

#BRAM_REF
startgroup
create_bd_cell -type ip -vlnv xilinx.com:ip:blk_mem_gen:8.4 blk_mem_gen_0
endgroup
set_property -dict [list CONFIG.Memory_Type {True_Dual_Port_RAM} CONFIG.Enable_B {Use_ENB_Pin} CONFIG.Use_RSTB_Pin {true} CONFIG.Port_B_Clock {100} CONFIG.Port_B_Write_Rate {50} CONFIG.Port_B_Enable_Rate {100}] [get_bd_cells blk_mem_gen_0]
set_property name BRAM_REF [get_bd_cells blk_mem_gen_0]

#BRAM_MV
startgroup
create_bd_cell -type ip -vlnv xilinx.com:ip:blk_mem_gen:8.4 blk_mem_gen_0
endgroup
set_property -dict [list CONFIG.Memory_Type {True_Dual_Port_RAM} CONFIG.Enable_B {Use_ENB_Pin} CONFIG.Use_RSTB_Pin {true} CONFIG.Port_B_Clock {100} CONFIG.Port_B_Write_Rate {50} CONFIG.Port_B_Enable_Rate {100}] [get_bd_cells blk_mem_gen_0]
set_property name BRAM_MV [get_bd_cells blk_mem_gen_0]

#BRAM_CTRL_0
startgroup
create_bd_cell -type ip -vlnv xilinx.com:ip:axi_bram_ctrl:4.1 axi_bram_ctrl_0
endgroup
set_property -dict [list CONFIG.SINGLE_PORT_BRAM {1}] [get_bd_cells axi_bram_ctrl_0]

#BRAM_CTRL_1
startgroup
create_bd_cell -type ip -vlnv xilinx.com:ip:axi_bram_ctrl:4.1 axi_bram_ctrl_1
endgroup
set_property -dict [list CONFIG.SINGLE_PORT_BRAM {1}] [get_bd_cells axi_bram_ctrl_1]

#BRAM_CTRL_2
startgroup
create_bd_cell -type ip -vlnv xilinx.com:ip:axi_bram_ctrl:4.1 axi_bram_ctrl_2
endgroup
set_property -dict [list CONFIG.SINGLE_PORT_BRAM {1}] [get_bd_cells axi_bram_ctrl_2]

#connect BRAM_interfaces
connect_bd_intf_net [get_bd_intf_pins AXI_ARPS_IP_0/BRAM_CURR] [get_bd_intf_pins BRAM_CURR/BRAM_PORTB]
connect_bd_intf_net [get_bd_intf_pins AXI_ARPS_IP_0/BRAM_MV] [get_bd_intf_pins BRAM_MV/BRAM_PORTB]
connect_bd_intf_net [get_bd_intf_pins AXI_ARPS_IP_0/BRAM_REF] [get_bd_intf_pins BRAM_REF/BRAM_PORTB]

connect_bd_intf_net [get_bd_intf_pins axi_bram_ctrl_0/BRAM_PORTA] [get_bd_intf_pins BRAM_CURR/BRAM_PORTA]
connect_bd_intf_net [get_bd_intf_pins axi_bram_ctrl_1/BRAM_PORTA] [get_bd_intf_pins BRAM_REF/BRAM_PORTA]
connect_bd_intf_net [get_bd_intf_pins axi_bram_ctrl_2/BRAM_PORTA] [get_bd_intf_pins BRAM_MV/BRAM_PORTA]
connect_bd_net [get_bd_pins processing_system7_0/IRQ_F2P] [get_bd_pins AXI_ARPS_IP_0/interrupt]

#run connection automation
update_compile_order -fileset sources_1
startgroup
apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config { Clk_master {Auto} Clk_slave {Auto} Clk_xbar {Auto} Master {/processing_system7_0/M_AXI_GP0} Slave {/AXI_ARPS_IP_0/s00_axi} intc_ip {New AXI Interconnect} master_apm {0}}  [get_bd_intf_pins AXI_ARPS_IP_0/s00_axi]
#INFO: [Ipptcl 7-1463] No Compatible Board Interface found. Board Tab not created in customize GUI
#</AXI_ARPS_IP_0/s00_axi/reg0> is being mapped into </processing_system7_0/Data> at <0x43C00000 [ 64K ]>
apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config { Clk_master {Auto} Clk_slave {Auto} Clk_xbar {Auto} Master {/processing_system7_0/M_AXI_GP0} Slave {/axi_bram_ctrl_0/S_AXI} intc_ip {Auto} master_apm {0}}  [get_bd_intf_pins axi_bram_ctrl_0/S_AXI]
#</axi_bram_ctrl_0/S_AXI/Mem0> is being mapped into </processing_system7_0/Data> at <0x40000000 [ 8K ]>
apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config { Clk_master {Auto} Clk_slave {Auto} Clk_xbar {Auto} Master {/processing_system7_0/M_AXI_GP0} Slave {/axi_bram_ctrl_1/S_AXI} intc_ip {Auto} master_apm {0}}  [get_bd_intf_pins axi_bram_ctrl_1/S_AXI]
#</axi_bram_ctrl_1/S_AXI/Mem0> is being mapped into </processing_system7_0/Data> at <0x42000000 [ 8K ]>
apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config { Clk_master {Auto} Clk_slave {Auto} Clk_xbar {Auto} Master {/processing_system7_0/M_AXI_GP0} Slave {/axi_bram_ctrl_2/S_AXI} intc_ip {Auto} master_apm {0}}  [get_bd_intf_pins axi_bram_ctrl_2/S_AXI]
#</axi_bram_ctrl_2/S_AXI/Mem0> is being mapped into </processing_system7_0/Data> at <0x44000000 [ 8K ]>
endgroup

startgroup
set_property range 64K [get_bd_addr_segs {processing_system7_0/Data/SEG_axi_bram_ctrl_0_Mem0}]
set_property range 64K [get_bd_addr_segs {processing_system7_0/Data/SEG_axi_bram_ctrl_1_Mem0}]
set_property range 4K [get_bd_addr_segs {processing_system7_0/Data/SEG_axi_bram_ctrl_2_Mem0}]
endgroup

validate_bd_design

#Creating hdl wrapper
make_wrapper -files [get_files $resultDir/arps.srcs/sources_1/bd/arps_bl_dsg/arps_bl_dsg.bd] -top

add_files -norecurse $resultDir/arps.srcs/sources_1/bd/arps_bl_dsg/hdl/arps_bl_dsg_wrapper.vhd
update_compile_order -fileset sources_1

#running synthesis and implementation
launch_runs impl_1 -to_step write_bitstream -jobs 2

#exporting hardware
wait_on_run impl_1
update_compile_order -fileset sources_1
file mkdir $resultDir/arps.sdk
file copy -force $resultDir/arps.runs/impl_1/arps_bl_dsg_wrapper.sysdef $resultDir/arps.sdk/arps_bl_dsg_wrapper.hdf